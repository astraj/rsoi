#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, redirect, url_for
import requests
import json


app = Flask(__name__)

def get_session_url(url_part):
    return "http://localhost:27012/" + url_part

def get_user_url(url_part):
    return "http://localhost:27013/" + url_part

def get_shops_url(url_part):
    return "http://localhost:27014/" + url_part

def get_orders_url(url_part):
    return "http://localhost:27015/" + url_part

def get_product_url(url_part):
    return "http://localhost:27016/" + url_part


def check_connection(login, token):
    url = get_session_url("check_connection?login={0}&token={1}".format(login, token))
    result = request.get(url).json()
    return 'ok' in result


@app.route("/add_user", methods=['POST'])
def add_user():
    data_json = request.get_json()
    url = get_user_url("add_user")
    headers = {'Content-type': 'application/json'}
    try:
        result = requests.post(url, data=json.dumps(data_json), headers=headers).json()
    except:
        msg = ('Временно невозможно зарегистировать пользователя').decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})
    json1 = json.dumps(result)
    return json1


@app.route("/authorize", methods=['GET'])
def authorize():
    username = request.args.get('username')
    password = request.args.get('password')
    if username is None or password is None:
        msg = ('Неправильно имя пользователя или пароль').decode('utf-8')
        return json.dumps({'error_code': 400, 'error_msg':  msg}, indent=4), 400
    url = get_session_url("authorize") + "?username={0}&password={1}".format(username, password)
    result = requests.get(url).json()
    json1 = json.dumps(result)
    return json1


####User info#################################################################################
@app.route("/get_me", methods=['GET'])
def get_me():
    username = request.args.get('name')
    url = get_user_url("get_me") + "?name={0}".format(username)
    try:
        result = requests.get(url).json()
    except:
        msg = ('Сервис временно недоступен').decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})
    json1 = json.dumps(result)
    return json1

@app.route("/add_shop", methods=['POST'])
def add_shop():
    data_json = request.get_json()

    url = get_shops_url("shop/")+data_json['name']
    headers = {'Content-type': 'application/json'}
    try:
        result = requests.get(url, data=json.dumps(data_json), headers=headers).json()
    except:
        return json.dumps({'error_code': 503, 'error_msg': 'Shops Service Temporary Unavailable'})
    if 'error_code' not in result:
        msg = ("Такой магазин уже зарегистрирован!").decode('utf-8')
        return json.dumps({'message': msg, 'error': 400})

    url = get_shops_url("add_shop")
    headers = {'Content-type': 'application/json'}
    try:
        result = requests.post(url, data=json.dumps(data_json), headers=headers).json()
    except:
        msg = ('Временно невозможно добавить магазин').decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})
    json1 = json.dumps(result)
    return json1


@app.route("/get_myshops", methods=['GET'])
def get_myshops():
    data_json = request.get_json()
    url = get_shops_url("get_myshops")
    headers = {'Content-type': 'application/json'}
    try:
        result = requests.get(url, data=json.dumps(data_json), headers=headers).json()
    except:
        msg = ('Сервис магазинов временно недоступен').decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})

    if 'error_code' in result:
        msg = ("Нет загеристрированных магазинов").decode('utf-8')
        return json.dumps({'message': msg, 'error': 400}, indent=4), 400

    json1 = json.dumps(result)
    return json1

@app.route("/shop/<id>", methods=['GET', 'PUT', 'DELETE'])
def get_shop(id):
    url = get_shops_url("shop/")+id
    headers = {'Content-type': 'application/json'}
    try:
        if request.method == "GET":
            result = requests.get(url, headers=headers).json()
        elif request.method == "PUT":
            data_json = request.get_json()
            result = requests.put(url, headers=headers, data=json.dumps(data_json)).json()
        elif request.method == "DELETE":
            data_json = request.get_json()
            result = requests.delete(url, headers=headers, data=json.dumps(data_json)).json()
    except:
        msg = ("Сервис временно недоступен").decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})

    if 'error_code' in result:
        msg = result['error_msg']
        return json.dumps({'message': msg, 'error': 400}, indent=4), 400

    json1 = json.dumps(result)
    return json1

@app.route("/shoporder/<id>", methods=['GET', 'PUT'])
def shop_order(id):
    url = get_orders_url("shoporder/")+id
    headers = {'Content-type': 'application/json'}
    try:
        if request.method == "GET":
            result = requests.get(url, headers=headers).json()
        elif request.method == "PUT":
            data_json = request.get_json()
            result = requests.put(url, headers=headers, data=json.dumps(data_json)).json()
        elif request.method == "DELETE":
            data_json = request.get_json()
            result = requests.delete(url, headers=headers, data=json.dumps(data_json)).json()
    except:
        msg = ("Сервис временно недоступен").decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})

    if 'error_code' in result:
        msg = result['error_msg']
        return json.dumps({'message': msg, 'error': 400}, indent=4), 400

    json1 = json.dumps(result)
    return json1

####search_product #################################################################################
@app.route("/get_product/<id>", methods=['GET'])
def get_product(id):
    data_json = request.get_json()
    url = get_product_url("product/")+id
    headers = {'Content-type': 'application/json'}
    try:
        result = requests.get(url, data=json.dumps(data_json), headers=headers).json()
    except:
        msg = ('Сервис товаров временно недоступен').decode('utf-8')
        return json.dumps({'error': 503, 'error_msg': msg})

    if 'error_code' in result:
        return json.dumps({'message': result["error_msg"], 'error': 400}, indent=4), 400

    json1 = json.dumps(result)
    return json1

@app.route("/add_products/<shop>", methods=['POST'])
def add_products(shop):
    data_json = request.get_json()
    url = get_product_url("add_products/")+shop
    headers = {'Content-type': 'application/json'}
    try:
        result = requests.post(url, data=json.dumps(data_json), headers=headers).json()
    except:
        msg = ('Сервис товаров временно недоступен').decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})

    if 'error_code' in result:
        return json.dumps({'message': result["error_msg"], 'error': 400})

    json1 = json.dumps(result)
    return json1

@app.route("/delete_product/<code>", methods=['DELETE'])
def delete_products(code):
    data_json = request.get_json()
    url = get_product_url("/delete_products/")+code
    headers = {'Content-type': 'application/json'}
    try:
        result = requests.delete(url, data=json.dumps(data_json), headers=headers).json()
    except:
        msg = ('Сервис товаров временно недоступен').decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})

    if 'error_code' in result:
        return json.dumps({'message': result["error_msg"], 'error': 400}, indent=4), 400

    json1 = json.dumps(result)
    return json1

@app.route("/edit_product/<code>", methods=['PUT'])
def edit_products(code):
    data_json = request.get_json()
    url = get_product_url("/edit_product/")+code
    headers = {'Content-type': 'application/json'}
    try:
        result = requests.put(url, data=json.dumps(data_json), headers=headers).json()
    except:
        msg = ('Сервис товаров временно недоступен').decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})

    if 'error_code' in result:
        return json.dumps({'message': result["error_msg"], 'error': 400}, indent=4), 400

    json1 = json.dumps(result)
    return json1

@app.route("/products_list/<id>", methods=['GET'])
def products_list(id):
    data_json = request.get_json()
    url = get_product_url("products_list/")+id
    headers = {'Content-type': 'application/json'}
    try:
        result = requests.get(url, data=json.dumps(data_json), headers=headers).json()
    except:
        msg = ('Сервис товаров временно недоступен').decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})

    if 'error_code' in result:
        return json.dumps({'message': result["error_msg"], 'error': 400}, indent=4), 400

    json1 = json.dumps(result)
    return json1

#### #################################################################################
@app.route("/check_session", methods=['GET'])
def check_ss():
    username = request.args.get('name')
    code = request.args.get('code')
    url = get_session_url("check_session") + "?username={0}&code={1}".format(username, code)
    try:
        result = requests.get(url).json()
    except:
        msg = ('Сервис временно недоступен').decode('utf-8')
        return json.dumps({'error_code': 503, 'error_msg': msg})
    json1 = json.dumps(result)
    return json1

if __name__ == "__main__":
    app.run(debug=True, port=27011)