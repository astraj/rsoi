#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify
import random
import string
import json
from db import *

app = Flask(__name__)

@app.route("/authorize", methods=['GET'])
def authorize():
    user_name = request.args.get('username')
    password = request.args.get('password')
    if user_exist(user_name, password):
        code = ''.join(random.choice(string.lowercase) for i in range(30))
        i = insert_code(code, user_name)
        if i == 0:
            return json.dumps({'error_code': 500, 'error_msg': 'Database Error'})
        return json.dumps({'code': code})
    return json.dumps({'error_code': 400, 'error_msg': 'Bad Request'})


@app.route("/check_session", methods=['GET'])
def check_ss():
    user_name = request.args.get('username')
    code = request.args.get('code')
    i = user_connected(user_name, code)
    if i == 0:
        return json.dumps({'error_code': 401, 'error_msg': 'UnAuthorized'})
    else:
        return json.dumps({'ok': 'ok'})

if __name__ == "__main__":
    app.run(debug=True, port=27012)