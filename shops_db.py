#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pyodbc
import pre

def shops_db_conn():
    cnxn = pyodbc.connect(r'DRIVER={SQL Server};' +
            r'SERVER=(local)\SQLEXPRESS;' +
            r'Database=shops;' +
            r'UID=computer;PWD=Rsoi2016')
    cursor = cnxn.cursor()
    return cursor

def shop_by_name(name):
    cursor = shops_db_conn()
    cursor.execute("select * from shops where name='" + name+"'")
    row = cursor.fetchone()
    if row:
        return row
    else:
        return 0

def add_shop_db(username, name, price, email, license, telephone, site):
    cursor = shops_db_conn()
    mystr = ("insert into shops "+"([name],[price],[telephone],[email],[site],[license]) values ('")
    mystr = (mystr +name+"', '"+price+"', '"+telephone+"', '"+email+"', '"+site+"', '"+license+"')")
    mystr2 = ("insert into owner_shop " + "([shop],[username]) values ('"+name+"', '"+username+"')")
    try:
        cursor.execute(mystr)
        cursor.commit()

        cursor.execute(mystr2)
        cursor.commit()

    except pyodbc.IntegrityError:
        return 0
    return 1

def update_shop(name, price, telephone ,email, site,license):
    cursor = shops_db_conn()
    mystr = ("update shops set price='"+price+"', telephone ='"+telephone+"', email = '"+email)
    mystr = (mystr+ "', site = '"+site+ "', license = '"+license+"' where name='"+name+"'")
    try:
        cursor.execute(mystr)
        cursor.commit()
    except pyodbc.IntegrityError:
        return 0
    return 1

def delete_shop(shop, username):
    cursor = shops_db_conn()
    mystr = ("delete from shops where name='"+shop+"'")
    mystr2 = ("delete from owner_shop where shop='"+shop+"' and username='"+username+"'")
    try:
        cursor.execute(mystr)
        cursor.commit()

        cursor.execute(mystr2)
        cursor.commit()
    except pyodbc.IntegrityError:
        return 0
    return 1


def user_shops(username):
    items = []
    cursor = shops_db_conn()
    cursor.execute("select * from owner_shop where username='" + username+"'")
    rows = cursor.fetchall()
    if len(rows) > 0:
        for row in rows:
            cursor.execute("select * from shops where name='" + row.shop+"'")
            shoprows = cursor.fetchone()
            items.append({
                'name': shoprows.name,
                'price': shoprows.price,
                'phone': shoprows.telephone,
                'email': shoprows.email,
                'site': shoprows.site,
                'license': shoprows.license
                })
        return items
    else:
        return None

def shops_price():
    items = []
    cursor = shops_db_conn()
    cursor.execute("select name, price from shop")
    rows = cursor.fetchall()
    if len(rows) > 0:
        for row in rows:
            items.append({
                'name': row.name,
                'price': row.price,
                })
        return items
    else:
        return None