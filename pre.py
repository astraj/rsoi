import json

class User(object):
	def __init__(self, login=None, name=None, lname=None, email=None, phone=None):
		self.login = login
		self.name = name
		self.last_name = lname
		self.email = email
		self.phone = phone
	def validate(self):
		if self.name is None or self.login is None or self.email is not None:
			return False
		return True
	def to_json(self):
		return json.dumps({
				'name':self.login,
				'fname':self.name,
				'lname':self.last_name,
				'tel':self.phone,
				'email':self.email})

class Order(object):
	def __init__(self,status=None,number=None,pcode=None,
				 pname=None,shop=None,user_id=None,price=None,address=None,
				 reg_date=None,delivery_date=None,payment=None,comment=None):
		self.status = status
		self.number = number
		self.product_code = pcode
		self.product_name = pname
		self.shop = shop
		self.user_id = user_id
		self.price = price
		self.address = address
		self.reg_date = reg_date
		self.delivery_date = delivery_date
		self.payment = payment
		self.comment = comment
	def validate(self):
		#if (self.status is None or self.number is None or self.product_code is None or self.product_name is None
		#	or self.shop is None or self.user_id is None or self.address is None):
		#		return False
		for a in self.__dict__:
			if a is None:
				return False
		return True
	def to_json(self):
		return json.dumps({
		'status' :self.status,
		'number' :self.number,
		'pcode':self.product_code,
		'pname': self.product_name,
		'shop': self.shop,
		'user_id': self.user_id,
		'price':self.price,
		'address':self.address,
		'reg_date':self.reg_date,
		'delivery_date': self.delivery_date,
		'payment':self.payment,
		'comment':self.comment})
	def from_json(self, json):
		self.status = json['status']
		self.number = json['number']
		self.product_code= json['product_code']
		self.product_name = json['product_name']
		self.shop = json['shop']
		self.user_id = json['user_id']
		self.address = json['address']
		self.reg_date = json['reg_date']
		self.delivery_date = json['delivery_date']
		self.payment = json['payment']
		self.comment = json['comment']
	def from_form(self, request):
		self.status = request.form['status']
		self.number = request.form['number']
		self.product_code= request.form['pcode']
		self.product_name = request.form['pname']
		self.shop = request.form['shop']
		self.user_id = request.form['user_id']
		self.address = request.form['address']
		self.reg_date = request.form['reg_date']
		self.delivery_date = request.form['delivery_date']
		self.payment = request.form['payment']
		self.comment = request.form['comment']


class Shop(object):
	def __init__(self, name,email ,phone,price,site,lic):
		self.name = name
		self.email = email
		self.phone = phone
		self.price = price
		self.site = site
		self.license = lic
	def validate(self):
		if self.name is None or self.price is None or self.email is None or self.license is None:
			return False
		return True
	def to_json(self):
			return json.dumps({
		   'name': self.name,
		   'price': self.price,
		   'phone': self.phone,
		   'email': self.email,
		   'site': self.site,
		   'license': self.license})
