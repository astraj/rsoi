#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, session, redirect, url_for, make_response
import requests
import json
import os
from datetime import datetime, timedelta
from pre import *
#Rsoi2016
app = Flask(__name__, static_path='')


def get_logic_url(url_part):
	return "http://localhost:27011/" + url_part


def get_data_from_cookies():
	expired = session.get('expired')
	if expired is None:
		return None, None
	if expired < datetime.now():
		clear_data_in_cookies()
		return 0, 0
	return session.get('login'), session.get('code')


def set_data_to_cookies(user, code):
	session['login'] = user
	session['code'] = code
	session['expired'] = datetime.now() + timedelta(minutes=10)


def clear_data_in_cookies():
	session.pop('login', None)
	session.pop('code', None)
	session.pop('expired', None)

def check_session(name, code):
	if name is None or code is None:
		return json.dumps({'error_code': 401, 'error_msg': 'UnAuthorized'})
	if name == 0 or code == 0:
		return json.dumps({'error_code': 498, 'error_msg': 'Token expired'})
	url = get_logic_url("check_session") + "?name={0}&code={1}".format(name, code)
	try:
		result = requests.get(url).json()
	except:
		return json.dumps({'message': 'Service Logic Temporary Unavailable', 'error': 503}, indent=4)
	return result

################################################################################################################
@app.route("/")
def one():
	return redirect(url_for('main'))

@app.route("/main")
def main():
	name, code = get_data_from_cookies()
	result = check_session(name, code)
	if 'error_code' in result:
		auth = 0
	else:
		auth = 1
	return render_template('main.html', auth=auth)

@app.route("/authorize", methods=['GET', 'POST'])
def login():
	error_info = ''
	name, code = get_data_from_cookies()
	if name is None or code is None:
		if request.method == 'POST':
			if not ('login' in session and 'code' in session):
				username = request.form['username']
				password = request.form['password']
				url = get_logic_url("authorize") + "?username={0}&password={1}".format(username, password)
				try:
					result = requests.get(url).json()
				except:
					return json.dumps({'error_code': 503, 'error_msg': 'Service Session Temporary Unavailable'})
				if 'error_code' in result:
					code = result['error_code']
					msg = result['error_msg']
					return json.dumps({'message': msg, 'error': code}, indent=4), code
				session.permanent = True
				set_data_to_cookies(username, result['code'])
				response = make_response(redirect(url_for('main')))
				response.set_cookie('Expires', "{0}".format(datetime.now()+timedelta(minutes=10)))
				response.set_cookie('token', result['code'])
				response.set_cookie('login', username)
				return response
			return redirect(url_for('main'))
		return render_template('auth.html', errorInfo=error_info)
	return redirect(url_for('main'))


@app.route('/user_register', methods=['POST', 'GET'])
def user_register():
	error_info = ''
	name, code = get_data_from_cookies()
	if name is None or code is None:
		if request.method == 'POST':
			name = request.form['username']
			first_name = request.form['fname']
			last_name = request.form['lname']
			phone = request.form['tel']
			email = request.form['email']
			password = request.form['password']

			url = get_logic_url("add_user")
			data = {'name': name, 'password': password, 'email': email, 'phone': phone, 'fname': first_name, 'lname': last_name}
			headers = {'Content-type': 'application/json'}

			result = requests.post(url, data=json.dumps(data), headers=headers).json()

			if 'error_code' in result:
				code = result['error_code']
				msg = result['error_msg']
				return json.dumps({'message': msg, 'error': code}, indent=4)
			return render_template("auth.html")
		return render_template('user_register.html', errorInfo=error_info)
	return redirect(url_for('main'))


@app.route("/logout", methods=['GET'])
def logout():
	name, code = get_data_from_cookies()
	if name is None or code is None:
		return make_response(redirect(url_for('main')))#render_template("home.html")
	clear_data_in_cookies()
	response = make_response(redirect(url_for('main')))
	response.delete_cookie('login')
	response.delete_cookie('Expires')
	response.delete_cookie('token')
	return redirect(url_for('login'))

@app.route("/about", methods=['GET'])
def about():
	name, code = get_data_from_cookies()
	result = check_session(name, code)
	if 'error_code' in result:
		auth = 0
	else:
		auth = 1
	return render_template("about.html", auth=auth)

##User info##################################################################################################
@app.route("/userinfo", methods=['GET'])
def userinfo():
	name, code = get_data_from_cookies()
	if name is None or code is None:
		return redirect(url_for('login'))
	clear_data_in_cookies()
	response = make_response(redirect(url_for('main')))
	return render_template("userinfo.html")

@app.route('/userinfo/me', methods=['GET'])
def me():
	name, code = get_data_from_cookies()
	result = check_session(name, code)
	if 'error_code' in result:
		return redirect(url_for('login'))

	if request.method == 'GET':
		url = get_logic_url("get_me") + "?name={0}".format(name)

		try:
			result = requests.get(url).json()
		except:
			msg = ('Временно невозможно получить личные данные').decode('utf-8')
			return json.dumps({'message': msg, 'error': 503}, indent=4)

		if 'error_code' in result:
			code = result['error_code']
			msg = result['error_msg']
			return json.dumps({'message': msg, 'error': code}, indent=4)

		return render_template("me.html", user=result, auth=1)

	if request.method == 'POST':
		url = get_logic_url("update_user") + "?name={0}".format(name)
		return render_template("me.html", user=result, auth=1)


@app.route("/userinfo/myorder", methods=['GET'])
def myorder():
	name, code = get_data_from_cookies()
	result = check_session(name, code)

	if 'error_code' in result:
		return redirect(url_for('login'))

	if request.method == 'GET':
		url = get_logic_url("get_order") + "?name={0}".format(name)

		try:
			result = requests.get(url).json()
		except:
			msg = ("Сервис временно недоступен").decode('utf-8')
			return json.dumps({'message': msg, 'error': 503}, indent=4)

		if 'error_code' in result:
			code = result['error_code']
			msg = result['error_msg']
			return json.dumps({'message': msg, 'error': code}, indent=4)

		return render_template("myorder.html", user=result, auth=1)

@app.route("/userinfo/shoporder/<id>", methods=['GET','POST'])
def shoporder(id):
	name, code = get_data_from_cookies()
	result = check_session(name, code)

	if 'error_code' in result:
		return redirect(url_for('login'))

	if request.method == 'GET':
		url = get_logic_url("shop_order") + id

		try:
			result = requests.get(url).json()
		except:
			msg = ("Сервис временно недоступен").decode('utf-8')
			return json.dumps({'message': msg, 'error': 503}, indent=4)

		if 'error_code' in result:
			code = result['error_code']
			msg = result['error_msg']
			return json.dumps({'message': msg, 'error': code}, indent=4)

		return render_template("myorder.html", user=result, auth=1)
	if request.method == 'GET':
		return render_template("myorder.html", user=result, auth=1)


@app.route('/userinfo/myshops', methods=['GET'])
def get_myshops():
	name, code = get_data_from_cookies()
	result = check_session(name, code)
	if 'error_code' in result:
		return redirect(url_for('login'))

	if request.method == 'GET':
		url = get_logic_url("get_myshops")
		data = {'username': name}
		headers = {'Content-type': 'application/json'}

		try:
			result = requests.get(url, data=json.dumps(data), headers=headers).json()
		except:
			msg = ("Временно невозможно получить список магазинов").decode('utf-8')
			return render_template("myshops.html", shops=result, errorInfo=msg, auth=1)

		if 'error_code' in result:
			msg = ("Сервис временно недоступен").decode('utf-8')
			return render_template("myshops.html", shops=result, errorInfo=msg, auth=1)

		return render_template("myshops.html", shops=result, auth=1)

@app.route('/userinfo/delete_shop/<id>', methods=['GET'])
def delete_shop(id):
	username, code = get_data_from_cookies()
	result = check_session(username, code)
	if 'error_code' in result:
		return redirect(url_for('login'))

	url = get_logic_url("shop/")+id
	headers = {'Content-type': 'application/json'}
	data = {'username': username, 'shop': id}

	try:
		result = requests.delete(url, headers=headers, data=json.dumps(data)).json()
	except:
		msg = ("Временно невозможно редактировать данный магазин").decode('utf-8')
		return render_template("myshops.html", shop=result, errorInfo=msg, auth=1)

	if 'error_code' in result:
		msg = ("Сервис временно недоступен").decode('utf-8')
		return render_template("myshops.html", shop=result, errorInfo=msg, auth=1)

	return redirect(url_for('get_myshops'))

@app.route('/userinfo/myshops/<id>', methods=['GET','POST'])
def edit_shop(id):
	username, code = get_data_from_cookies()
	result = check_session(username, code)
	if 'error_code' in result:
		return redirect(url_for('login'))
	url = get_logic_url("shop/")+id
	headers = {'Content-type': 'application/json'}

	if request.method == 'GET':
		try:
			result = requests.get(url, headers=headers).json()
		except:
			msg = ("Временно невозможно получить информацию по данному магазину").decode('utf-8')
			return render_template("myshops.html", shop=result, errorInfo=msg, auth=1)

		if 'error_code' in result:
			msg = ("Сервис временно недоступен").decode('utf-8')
			return render_template("myshops.html", shop=result, errorInfo=msg, auth=1)

		return render_template("edit_shop.html", shop=result, auth=1)

	if request.method == 'POST':
		try:
			name = id
			price = request.form['price']
			phone = request.form['phone']
			email = request.form['email']
			site = request.form['site']
			license = request.form['license']
			data = {'name': name, 'price': price, 'telephone': phone, 'email': email,'site': site, 'license': license}
			result = requests.put(url, headers=headers, data=json.dumps(data)).json()
		except:
			msg = ("Временно невозможно редактировать данный магазин").decode('utf-8')
			return render_template("myshops.html", shop=result, errorInfo=msg, auth=1)

		if 'error_code' in result:
			msg = ("Сервис временно недоступен").decode('utf-8')
			return render_template("myshops.html", shop=result, errorInfo=msg, auth=1)

		msg = "Изменения успешно внесены"
		return render_template("edit_shop.html", successInfo=msg.decode('utf-8'),shop=result, auth=1)


##############shop###################################################################################################

@app.route('/shop_register', methods=['POST', 'GET'])
def shop_register():
	error_info = ''
	name, code = get_data_from_cookies()
	result = check_session(name, code)

	if 'error_code' in result:
		return redirect(url_for('login'))

	if request.method == 'POST':
		try:
			username = name
			name = request.form['name'].encode('utf-8')
			price = ''#request.form['price']
			phone = request.form['tel']
			email = request.form['email']
			site = request.form['site']
			license = request.form['license']
			pricefile = request.files['json']
		except:
			msg = ("Ошибка заполнения формы").decode('utf-8')
			return render_template("shop_register.html", warningInfo=msg, auth=1)

		url = get_logic_url("add_shop")
		data = {'username': username, 'name': name, 'price': price, 'telephone': phone, 'email': email,'site': site, 'license': license}
		headers = {'Content-type': 'application/json'}

		try:
			result = requests.post(url, data=json.dumps(data), headers=headers).json()
		except:
			msg = ("Временно невозможна регистрация магазина").decode('utf-8')
			return render_template("shop_register.html", warningInfo=msg, auth=1)

		if 'error_code' in result:
			msg = ("Временно невозможна регистрация магазина").decode('utf-8')
			return render_template("shop_register.html", warningInfo=msg, auth=1)

		if 'error' in result:
			msg = result['message']
			return render_template("shop_register.html", errorInfo=msg, auth=1)

		if pricefile.filename is not None:
			try:
					strfile = pricefile.stream.read()
					data = json.loads(strfile)
					url = get_logic_url("add_products/")+name
					headers = {'Content-type': 'application/json'}
					result = requests.post(url, data=json.dumps(data), headers=headers).json()
			except:
				msg = ("Магазин был зарегистирован, но не удалось загрузить прайс лист. Возможно, файл некорректно составлен").decode('utf-8')
				return render_template("shop_register.html", warningInfo=msg, auth=1)

			if 'error_code' in result:
				msg = result['error_msg']
				return render_template("shop_register.html", errorInfo=msg, auth=1)

		msg = "Регистрация магазина прошла успешно"
		return render_template("shop_register.html", successInfo=msg.decode('utf-8'))
	return render_template('shop_register.html', errorInfo=error_info, auth=1)

##############products###################################################################################################
@app.route('/userinfo/myshops/products/', methods=['GET'])
def list_products():
	username, code = get_data_from_cookies()
	result = check_session(username, code)
	if 'error_code' in result:
		return redirect(url_for('login'))
	url = get_logic_url("products_list/")
	headers = {'Content-type': 'application/json'}
	try:
		result = requests.get(url, headers=headers).json()
	except:
		msg = ("Временно невозможно редактировать данный товар").decode('utf-8')
		return render_template("myshops.html", shop=result, errorInfo=msg, auth=1)

	if 'error_code' in result:
		msg = ("Сервис временно недоступен").decode('utf-8')
		return render_template("myshops.html", shop=result, errorInfo=msg, auth=1)

	return render_template("shop_products.html", products=result, shop=id, auth=1)

@app.route('/userinfo/myshops/products/<shop>/<id>', methods=['POST','GET'])
def edit_product(id, shop):
	username, code = get_data_from_cookies()
	result = check_session(username, code)
	if 'error_code' in result:
		return redirect(url_for('login'))
	url = get_logic_url("products/")+shop+"/"+id
	headers = {'Content-type': 'application/json'}

	if request.method == 'GET':
		try:
			result = requests.get(url, headers=headers).json()
		except:
			msg = ("Временно невозможно получить информацию по данному магазину").decode('utf-8')
			return render_template("myshops.html", shop=shop, errorInfo=msg, auth=1)

		if 'error_code' in result:
			msg = ("Сервис временно недоступен").decode('utf-8')
			return redirect(url_for('list_products(' + shop + ')'))
		return render_template("shop_products.html", products=result, shop=shop, auth=1)

	if request.method == 'POST':
		try:
			result = requests.put(url, headers=headers).json()
		except:
			msg = ("Временно невозможно получить информацию по данному магазину").decode('utf-8')
			return redirect(url_for('list_products(' + shop + ')'))

		if 'error_code' in result:
			msg = ("Сервис временно недоступен").decode('utf-8')
			return render_template("myshops.html", shop=result, errorInfo=msg, auth=1)

		return render_template("shop_products.html", products=result, shop=id, auth=1)

@app.route('/userinfo/products/delete_product/<id>', methods=['POST'])
def delete_product(id):
	username, code = get_data_from_cookies()
	result = check_session(username, code)
	if 'error_code' in result:
		return redirect(url_for('login'))

	url = get_logic_url("delete_product/") + id
	headers = {'Content-type': 'application/json'}

	try:
		shop = request.form['shop']
		data = {'shop': shop}
		result = requests.delete(url, headers=headers, data=json.dumps(data)).json()
	except:
		msg = ("Временно невозможно удалить данный товар").decode('utf-8')
		return redirect(url_for('list_products('+shop+')'))

	if 'error_code' in result:
		msg = ("Временно невозможно удалить данный товар").decode('utf-8')
		return redirect(url_for('list_products(' + shop + ')'))

	msg = "Изменения успешно внесены"
	return render_template("edit_shop.html", successInfo=msg.decode('utf-8'), shop=result, auth=1)


##############search###################################################################################################
@app.route('/search/shop', methods=['GET'])
def search_shop():
	try:
		error_info = request.args.get('errorInfo')
	except:
		error_info = ''
	name, code = get_data_from_cookies()
	result = check_session(name, code)
	if 'error_code' in result:
		auth = 0
	else:
		auth = 1

	return render_template("search_shop.html", errorInfo=error_info, auth=auth)

@app.route('/search/shop/<id>', methods=['GET'])
def search_shop_id(id):
	try:
		error_info = request.args.get('errorInfo')
	except:
		error_info = ''
	name, code = get_data_from_cookies()
	result = check_session(name, code)
	if 'error_code' in result:
		auth = 0
	else:
		auth = 1

	url = get_logic_url("shop/") + id
	headers = {'Content-type': 'application/json'}
	try:
		result = requests.get(url, headers=headers).json()
	except:
		msg = ("Временно невозможно получить данные выбранного магазина").decode('utf-8')
		return redirect(url_for('search_shop', errorInfo=msg))

	if 'error' in result:
		msg = ("Временно невозможно получить данные выбранного магазина").decode('utf-8')
		return redirect(url_for('search_shop', errorInfo=msg))
	
	return render_template("shop_info.html", shop=result, auth=auth)


@app.route('/search/product', methods=['GET'])
def search_product():
	try:
		error_info = request.args.get('errorInfo')
	except:
		error_info = ''
	name, code = get_data_from_cookies()
	result = check_session(name, code)
	if 'error_code' in result:
		auth = 0
	else:
		auth = 1
	return render_template("search_product.html", auth=auth, errorInfo=error_info)

@app.route('/search/product/result', methods=['GET'])
def search_product_by_name():
	try:
		error_info = request.args.get('errorInfo')
	except:
		error_info = ''
	name, code = get_data_from_cookies()
	result = check_session(name, code)
	if 'error_code' in result:
		auth = 0
	else:
		auth = 1

	id = request.args.get("name").encode('utf-8')
	maxp = request.args.get("pricemax")
	minp = request.args.get("pricemin")
	type = request.args.get("type").encode('utf-8')
	company = request.args.get("company").encode('utf-8')
	url = get_logic_url("get_product/") + id
	headers = {'Content-type': 'application/json'}
	try:
		data = {'company': company, 'type': type, 'minp': minp, 'maxp': maxp}
		result = requests.get(url, headers=headers, data=json.dumps(data)).json()
	except:
		msg = ("Сервис товаров временно недоступен").decode('utf-8')
		return redirect(url_for('search_product',errorInfo=msg))
	if 'error' in result:
		msg = ("Не найдет товар по запросу").decode('utf-8')
		return redirect(url_for('search_product',errorInfo=msg))

	return render_template("products_result.html", products=result, auth=auth)

#####order###########################################################################################################
@app.route("/order/<id>", methods=['GET','PUT','POST'])
def order(id):
	url = get_logic_url("order/")+id
	headers = {'Content-type': 'application/json'}
	if request.method == 'POST':
		ord = Order()
		ord.from_form(request)
		data = order.to_json()
		try:
			result = requests.post(url, data=json.dumps(data), headers=headers).json()
		except:
			msg = ("Временно невозможно зарегистрировать заказ").decode('utf-8')
			#????return render_template("shop_register.html", warningInfo=msg, auth=1)

	if request.method == 'GET':
		result = requests.get(url, headers=headers).json()
	if request.method == 'PUT':
		ord = Order()
		ord.from_form(request)
		data = order.to_json()
		try:
			result = requests.put(url, data=json.dumps(data), headers=headers).json()
		except:
			msg = ("Временно невозможно редактировать заказ").decode('utf-8')


############################################################################################################
if __name__ == "__main__":
	page = 1
	app.secret_key = os.urandom(24)
	app.run(debug=True, port=27010)
