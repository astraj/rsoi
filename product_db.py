#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pyodbc
import json

def products_db_conn():
    cnxn = pyodbc.connect(r'DRIVER={SQL Server};' +
            r'SERVER=(local)\SQLEXPRESS;' +
            r'Database=products;' +
            r'UID=computer;PWD=Rsoi2016')
    cursor = cnxn.cursor()
    return cursor

def product_by_name(name, maxp, minp, type, company):
    items = []
    cursor = products_db_conn()
    query = ("select * from products where name like '%" + name + "%'")
    if maxp < minp and maxp != '' and minp != '':
        msg = ("Максимальная цена должна быть больше минимальной").decode('utf-8')
        return json.dumps({'error_code': 404, 'error_msg': msg})

    if maxp != '':
        if maxp > 0:
            query = query + " AND price < "+maxp
        else:
            msg = ("Цена должна быть больше 0").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})

    if minp != '':
        if minp > 0 :
            query = query +" AND price > "+ minp
        else:
            msg = ("Цена должна быть больше 0").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})

    if type != '':
        query = query +" AND type = '"+ type +"'"

    if company != '':
        query = query + " AND company = '" + company + "'"

    cursor.execute(query)
    rows = cursor.fetchall()
    if len(rows) > 0:
        for row in rows:
            product = row
            items.append({
                'code': product.code,
                'name': product.name,
                'price': str(product.price),
                'company': product.company,
                'type': product.type,
                'shop': product.shop
            })
        return items
    else:
        return None


def product_by_codeandshop(code, shop):
    items = []
    cursor = products_db_conn()
    cursor.execute("select * from products where code = '" + code+"'" + "and shop = '" + shop+"'")
    rows = cursor.fetchall()
    if len(rows) > 0:
        for row in rows:
            product = row
            items.append({
                'code': product.code,
                'name': product.name,
                'price': str(product.price),
                'company': product.company,
                'type': product.type,
                'shop': product.shop
            })
        return items
    else:
        return None

def product_by_shop(shop):
    items = []
    cursor = products_db_conn()
    cursor.execute("select * from products where shop = '" + shop+"'")
    rows = cursor.fetchall()
    if len(rows) > 0:
        for row in rows:
            product = row
            items.append({
                'code': product.code,
                'name': product.name,
                'price': product.price,
                'company': product.company,
                'type': product.type,
                'shop': product.shop
            })
        return items
    else:
        return None

def add_product(shop, code, name, price, type, company):
    items = []
    cursor = products_db_conn()
    mystr = ("insert into products " + "([code],[name],[price],[company],[type],[shop]) values ('")
    mystr2 = (mystr + str(code) + "', '" + name + "', " + str(price) + ", '" + company + "', '" + type + "', '" + shop + "')")
    try:
        cursor.execute(mystr2)
        cursor.commit()

    except pyodbc.IntegrityError:
        return 0
    return 1

def update_product(shop, code, name, price, type, company):
    items = []
    cursor = products_db_conn()
    mystr = ("update products set" + "([name],[price],[company],[type]) values (")
    mystr = (mystr + "'" + name + "', " + price + ", '" + company + "', '" + type + "',) ' where code = '"+code+"' and shop = '" + shop +"')")
    try:
        cursor.execute(mystr)
        cursor.commit()

    except pyodbc.IntegrityError:
        return 0
    return 1

def delete_product(shop, code):
    cursor = products_db_conn()
    mystr = ("delete from products where shop = '"+ shop +"' and code = '" + code+ "'")
    try:
        cursor.execute(mystr)
        cursor.commit()

    except pyodbc.IntegrityError:
        return 0
    return 1
