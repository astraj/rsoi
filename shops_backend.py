#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request
import requests
from pre import *
import json
from shops_db import *

app = Flask(__name__)

@app.route('/shop/<id>', methods=['GET', 'PUT', 'DELETE'])
def get_shop(id):
    if request.method == 'GET':
        row = shop_by_name(id)
        if row == 0:
            msg = ("Нет данного магазина").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})
        else:
            mshop = Shop(row.name, row.email, row.telephone, row.price, row.site, row.license)
            if mshop.validate():
                return mshop.to_json()
            else:
                msg = ("Ошибка при получении данных").decode('utf-8')
                return json.dumps({'error_code': 404, 'error_msg': msg})

    if request.method == 'PUT':
        data_json = request.get_json()
        name = data_json['name']
        price = data_json['price']
        phone = data_json['telephone']
        email = data_json['email']
        site = data_json['site']
        license = data_json['license']
        if name is None or price is None or email is None or license is None:
            raise Exception()
        row = update_shop(name, price, phone, email, site, license)
        if row == 0:
            msg = ("Не возможно редактировать данный магазин").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})
        else:
            return json.dumps({'ok': 'ok', 'code': 200})

    if request.method == 'DELETE':
        data_json = request.get_json()
        username = data_json['username']
        row = delete_shop(id, username)
        if row == 0:
            msg = ("Не возможно удалить данный магазин").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})
        else:
            return json.dumps({'ok': 'ok', 'code': 200})



@app.route("/add_shop", methods=['POST'])
def add_shop():
    if request.method == 'POST':
        data_json = request.get_json()
        try:
            name = data_json['name']
            price = data_json['price']
            telephone = data_json['telephone']
            email = data_json['email']
            site = data_json['site']
            license = data_json['license']
            username = data_json['username']
            if name is None or price is None or email is None or license is None:
                raise Exception()
        except:
            return json.dumps({'error_code': 400, 'error_msg': 'Bad Request'})
        i = add_shop_db(username, name, price, email, license, telephone, site)
        if i == 0:
            return json.dumps({'error_code': 400, 'error_msg': 'Bad Request'})
        return json.dumps({
            'ok': 'ok'}), 201, {
                'Content-Type': 'application/json;charset=UTF-8',
            }

@app.route('/get_myshops', methods=['GET'])
def get_myshops():
    my_json = request.get_json()
    username = my_json['username']
    result = user_shops(username)
    if result is None:
        msg = ("Нет зарегистрированных магазинов").decode('utf-8')
        return json.dumps({'error_code': 404, 'error_msg': msg})
    else:
        json1 = json.dumps(result)
        return json1

@app.route('/get_shops_price', methods=['GET'])
def get_shops_price():
    my_json = request.get_json()
    price_list = shops_price()
    if price_list is None:
        msg = ("Нет зарегистрированных магазинов").decode('utf-8')
        return json.dumps({'error_code': 404, 'error_msg': msg})
    else:
        json1 = json.dumps(price_list)
    return json1

@app.route('/get_product', methods=['GET'])
def get_product():
    my_json = request.get_json()
    items = []
    #product = my_json['product']
    #minprice = my_json['minprice']
    #maxprice = my_json['minprice']
    headers = {'Content-type': 'application/json'}
    #result = user_shops(username)
    price_list = get_shops_price()
    if price_list is None:
        msg = ("Нет зарегистрированных магазинов").decode('utf-8')
        return json.dumps({'error_code': 404, 'error_msg': msg})
    else:
        for price in price_list:
            url = price['price']
            try:
                result = requests.get(url, data=json.dumps(my_json), headers=headers).json()
            except:
                json.dumps({'error_code': 503, 'error_msg': ''})
            try:
                result['name']
                result['price']
                result['product_id']
                items.append(result)
            except:
                json.dumps({'error_code': 503, 'error_msg': ''})
        if len(items) > 0:
            json1 = json.dumps(items)
        #json1 = json.dumps(price_list)
            return json1
        else:
            msg = ("Не найден запрощенный товар").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})

if __name__ == "__main__":
    app.run(debug=True, port=27014)