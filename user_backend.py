#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify
import random
import string
import json
from db import *

app = Flask(__name__)

@app.route("/get_me", methods=['GET'])
def me():
    name = request.args.get('name')
    row = get_me(name)
    if row == 0:
        return json.dumps({'error_code': 500, 'error_msg': 'Server Error'})
    return jsonify(name=row.username,
                fname=row.firstname,
                lname=row.lastname,
                tel=row.telephone,
                email=row.email,
                password=row.password)

@app.route("/add_user", methods=['POST'])
def add_user_db():
    my_json = request.get_json()
    username = my_json['name']
    fname = my_json['fname']
    lname = my_json['lname']
    tel = my_json['phone']
    email = my_json['email']
    password = my_json['password']
    if user_exist(username, password):
        return json.dumps({'error_code': 400, 'error_msg': 'User already exists'}, {
        'Content-Type': 'application/json;charset=UTF-8',
    })
    try:
        insert_user(username, fname, lname, tel, email, password)
    except:
        return json.dumps({'error_code': 500, 'error_msg': 'Database error'})
    return json.dumps({'ok': 'ok'})


if __name__ == "__main__":
    app.run(debug=True, port=27013)