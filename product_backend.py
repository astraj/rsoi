#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request
from product_db import *
from pre import *
import json

app = Flask(__name__)

@app.route('/product/<id>', methods=['GET'])
def get_product(id):
    my_json = request.get_json()
    try:
        maxp = my_json["maxp"]
        minp = my_json["minp"]
        type = my_json["type"]
        company = my_json["company"]
    except:
        list = []

    list = product_by_name(id, maxp, minp, type, company)
    if list is None:
        msg = ("Нет данного продукта").decode('utf-8')
        return json.dumps({'error_code': 404, 'error_msg': msg})
    else:
        json1 = json.dumps(list)
        return json1

@app.route('/product_by_code/<id>', methods=['GET'])
def get_product_by_code(id):
    my_json = request.get_json()
    code = my_json["code"]
    shop = my_json["shop"]
    list = product_by_codeandshop(code, shop)
    if len(list) == 0:
        msg = ("Нет данного продукта").decode('utf-8')
        return json.dumps({'error_code': 404, 'error_msg': msg})
    else:
        return json.dumps(list)

@app.route('/product_by_shop', methods=['GET'])
def get_product_by_shop(id):
    my_json = request.get_json()
    code = my_json["code"]
    shop = my_json["shop"]
    list = product_by_codeandshop(code, shop)
    if len(list) == 0:
        msg = ("Нет товаров у данного магазина").decode('utf-8')
        return json.dumps({'error_code': 404, 'error_msg': msg})
    else:
        return json.dumps(list)

@app.route('/add_products/<shop>', methods=['POST'])
def add_products(shop):
    error_list = []
    my_json = request.get_json()
    jj = json.dumps({'ok': 'ok'})
    products = my_json["products"]
    for product in products:
        code = product["code"]
        name = product["name"]
        price = product["price"]
        company = product["company"]
        type = product["type"]
        list = add_product(shop, code, name, price, type, company)
        if list is not 1:
            error_list.append(product)
            jj = json.dumps(list)
    return jj

@app.route('/delete_product/<code>', methods=['DELETE'])
def delete_products(code):
    my_json = request.get_json()
    shop = my_json["shop"]
    product = delete_product(shop, code)
    if product == 1:
        return json.dumps({'ok': 'ok', 'code': 200})
    else:
        msg = ("Временно невозможно удалить товар. Возможно его уже нет в списке").decode('utf-8')
        return json.dumps({'error_code': 404, 'error_msg': msg})

@app.route('/edit_product/<code>', methods=['PUT'])
def edit_products(code):
    my_json = request.get_json()
    shop = my_json["shop"]
    name = my_json["name"]
    price = my_json["price"]
    type = my_json["type"]
    company = my_json["company"]

    product = update_product(shop, code, name, price, type, company)

    if product == 1:
        return json.dumps({'ok': 'ok', 'code': 200})
    else:
        msg = ("Временно невозможно редактировать товар").decode('utf-8')
        return json.dumps({'error_code': 404, 'error_msg': msg})

if __name__ == "__main__":
    app.run(debug=True, port=27016)