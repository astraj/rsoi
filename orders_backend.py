#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify

import json
from order_db import *
from pre import *

app = Flask(__name__)


@app.route('/order/<id>', methods=['GET', 'PUT', 'DELETE'])
def get_order(id):
    if request.method == 'GET':
        row = order_by_number(id)
        if row == 0:
            msg = ("Нет данного заказа").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})
        else:
            return json.dumps({
           'name': row.name})

    if request.method == 'PUT':
        data_json = request.get_json()
        m_order = Order()
        m_order.from_json(data_json)

        if m_order.validate():
            raise Exception()
        row = update_order(m_order)
        if row == 0:
            msg = ("Не возможно редактировать данный магазин").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})
        else:
            return json.dumps({'ok': 'ok', 'code': 200})

    if request.method == 'DELETE':
        data_json = request.get_json()
        username = data_json['username']
        row = delete_order(id, username)
        if row == 0:
            msg = ("Не возможно удалить данный магазин").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})
        else:
            return json.dumps({'ok': 'ok', 'code': 200})



@app.route("/add_order", methods=['POST'])
def add_order():
    if request.method == 'POST':
        data_json = request.get_json()
        try:
            m_order = Order()
            m_order.from_json(data_json)
            if m_order.validate():
                raise Exception()
        except:
            return json.dumps({'error_code': 400, 'error_msg': 'Bad Request'})
        i = add_order(m_order)
        if i == 0:
            return json.dumps({'error_code': 400, 'error_msg': 'Bad Request'})
        return json.dumps({
            'ok': 'ok'}), 201, {
                'Content-Type': 'application/json;charset=UTF-8'  }

@app.route('/get_myorders', methods=['GET'])
def get_myorders():
    if request.method == 'GET':
        my_json = request.get_json()
        username = my_json['username']
        result = user_orders(username)
        if result is None:
            msg = ("Нет зарегистрированных магазинов").decode('utf-8')
            return json.dumps({'error_code': 404, 'error_msg': msg})
        else:
            json1 = json.dumps(result)
            return json1

if __name__ == "__main__":
    app.run(debug=True, port=27015)